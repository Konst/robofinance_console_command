Robofinance test task
===============

This project uses Symfony 3.4 version.


I used homestead virtual machine with nginx and PHP7. You can use any environment, just don't forget to configure nginx as it described on the symfony documentation page 
https://symfony.com/doc/3.4/setup/web_server_configuration.html

How to install application
-------

0. Prepare your environment(nginx, mysql)
1. Create database (But don't create any tables). For example let it named 'robofinance'
2. Pull repo
3. Create parameters file (app/config/parameters.yml) next to the app/config/parameters.yml.dist 
4. Run composer install - it generate all necessary files
5. Run php bin/console doctrine:schema:update --force (it will update your empty database and create 'users' table);
6. Add users to database


How to run a command
------

Run the next command (from project root directory): 
php bin/console app:send-money 'sender id 'receiver id' 'sum'


