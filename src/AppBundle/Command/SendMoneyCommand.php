<?php

namespace AppBundle\Command;

use AppBundle\Service\TransactionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class SendMoneyCommand extends Command
{

    private $transactionService;

    public function __construct(string $name = null, TransactionService $transactionService)
    {
        parent::__construct($name);
        $this->transactionService = $transactionService; //Inject custom transaction service.
    }

    protected function configure()
    {
        $this
            ->setName('app:send-money')
            ->setDescription('Move money from one user to another, enter sender id, reciever id and summ')
            ->setHelp('This command allows you to send money...')
            ->addArgument('sender_id', InputArgument::REQUIRED, 'The id of the sender.')
            ->addArgument('receiver_id', InputArgument::REQUIRED, 'The id of the receiver.')
            ->addArgument('sum', InputArgument::REQUIRED, 'The sum.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //Getting arguments from console
        $params = $input->getArguments();
        $output->writeln('Sending money...');

        //run transaction to send transfer. It moved to separate service (transactionService).
        $transactionService = $this->transactionService;
        $transactionService->sendUserToUserMoneyTransfer($params['sender_id'], $params['receiver_id'], $params['sum']);

        $output->writeln(
            [
                'Transaction has been successfully completed!',
                'Transaction details: 
                Sender id = '.$params['sender_id'].' Sum = '.$params['sum'].' Receiver = '.$params['receiver_id'],
            ]
        );
    }
}
