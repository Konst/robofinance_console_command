<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

class TransactionService
{
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function sendUserToUserMoneyTransfer($sender_id, $receiver_id, $sum)
    {
        $em = $this->em;
        $em->getConnection()->beginTransaction();
        try {

            if ($sender_id === $receiver_id) {
                throw new \Exception('Sender and receiver it\'s the same person!');
            }

            //get sender Entity
            $userRepo = $em->getRepository('AppBundle:Users');
            $sender = $userRepo->findOneBy(['id' => $sender_id]);
            if (!$sender) {
                throw new \Exception('Sender doesn\'t exist!');
            }

            if ($sender->getBalance() < $sum) {
                throw new \Exception('Sender doesn\'t have enough money!');
            }

            //get Reciever Entity
            $receiver = $userRepo->findOneBy(['id' => $receiver_id]);
            if (!$receiver) {
                throw new \Exception('Receiver doesn\'t exist!');
            }

            $sender->setBalance($sender->getBalance() - $sum);
            $receiver->setBalance($receiver->getBalance() + $sum);

            $em->flush();
            $em->getConnection()->commit();
        } catch (Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
    }

}

